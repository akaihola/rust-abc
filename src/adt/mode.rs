#[derive(Debug, PartialEq)]
pub enum Mode {
    Major,
    Dorian,
    Phrygian,
    Lydian,
    Mixolydian,
    Minor,
    Locrian
}

impl Mode {
    pub fn from_str(string: &str) -> Result<Mode, String> {
        use self::Mode::*;
        let string = string.trim().to_lowercase();
        let string = string.as_str();

        let ret = match string {
            "" => Major,
            "m" => Minor,
            _ => {
                if string.len() < 3 {Err(format!("{} is not a valid mode", string))?};
                let string = &string[0..3];
                match string {
                    "maj" => Major,
                    "dor" => Dorian,
                    "phr" => Phrygian,
                    "lyd" => Lydian,
                    "mix" => Mixolydian,
                    "min" => Minor,
                    "loc" => Locrian,
                    _ => Err(format!("{} is not a valid mode", string))?
                }
            }
        };
        Ok(ret)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::Mode::*;

    #[test]
    fn maj_1() {
        let m = Mode::from_str("").unwrap();
        assert_eq!(m, Major)
    }

    #[test]
    fn maj_2() {
        let m = Mode::from_str("maj").unwrap();
        assert_eq!(m, Major)
    }

    #[test]
    fn maj_3() {
        let m = Mode::from_str("Maj").unwrap();
        assert_eq!(m, Major)
    }

    #[test]
    fn maj_4() {
        let m = Mode::from_str("major").unwrap();
        assert_eq!(m, Major)
    }

    #[test]
    fn maj_5() {
        let m = Mode::from_str(" major").unwrap();
        assert_eq!(m, Major)
    }

    #[test]
    fn dor() {
        let m = Mode::from_str("dorian").unwrap();
        assert_eq!(m, Dorian)
    }

    #[test]
    fn phr() {
        let m = Mode::from_str("phrygian").unwrap();
        assert_eq!(m, Phrygian)
    }

    #[test]
    fn lyd() {
        let m = Mode::from_str("lydian").unwrap();
        assert_eq!(m, Lydian)
    }

    #[test]
    fn mix() {
        let m = Mode::from_str("mixolydian").unwrap();
        assert_eq!(m, Mixolydian)
    }

    #[test]
    fn min() {
        let m = Mode::from_str("minor").unwrap();
        assert_eq!(m, Minor)
    }

    #[test]
    fn min_2() {
        let m = Mode::from_str("m").unwrap();
        assert_eq!(m, Minor)
    }

    #[test]
    fn loc() {
        let m = Mode::from_str("locrian").unwrap();
        assert_eq!(m, Locrian)
    }
}
