use parser::Parser;
use parser::ast::Ast;
use super::info_field::InfoField;


#[derive(Debug, PartialEq)]
pub struct TuneBook {
    tunes: Vec<Tune>
}

impl TuneBook {
    pub fn from_string(input: &str) -> Result<TuneBook, String> {
        let mut tune_book = TuneBook { tunes: vec![] };

        let mut parser = Parser::new(input);
        let mut ast = parser.parse()?;

        for child in ast.unwrap_children().iter() {
            let child_contents = &**child;
            match child_contents {
                Ast::InfoField('X', _) => {
                    tune_book.tunes.push(Tune {
                        header: TuneHeader {
                            info_fields: vec![InfoField::from_ast(child_contents)?]
                        },
                        body: None
                    })
                },
                Ast::InfoField(_, _) => {
                    let mut curr_tune = match tune_book.tunes.last_mut() {
                        Some(t) => t,
                        None => Err(format!("{:?} found outside of the context of a tune", child_contents))?
                    };
                    curr_tune.header.info_fields.push(
                        InfoField::from_ast(child_contents)?
                    )
                },
                _ => ()
            }
            println!("{:?}", child);
        }
        Ok(tune_book)
    }
}

#[derive(Debug, PartialEq)]
pub struct Tune {
    header: TuneHeader,
    body: Option<TuneBody>
}

#[derive(Debug, PartialEq)]
pub struct TuneHeader {
    info_fields: Vec<InfoField>
}

#[derive(Debug, PartialEq)]
pub struct TuneBody {

}

#[cfg(test)]
mod tests {
    use adt::{TuneBook, Tune, TuneHeader, InfoField, Key, Mode};

    #[test]
    fn tune_simple() {
        let tune_book = TuneBook::from_string("X:1\n").unwrap();
        let expected = TuneBook {
            tunes: vec![
                Tune {
                    header: TuneHeader {
                        info_fields: vec![InfoField::ReferenceNumber(1)]
                    },
                    body: None
                }
            ]
        };

        assert_eq!(tune_book, expected);
    }

    #[test]
    fn tune_key_field() {
        let tune_book = TuneBook::from_string("X:1\nK:G\n").unwrap();
        let expected = TuneBook {
            tunes: vec![
                Tune {
                    header: TuneHeader {
                        info_fields: vec![
                            InfoField::ReferenceNumber(1),
                            InfoField::Key(Key::G(Mode::Major))
                        ]
                    },
                    body: None
                }
            ]
        };

        assert_eq!(tune_book, expected);
    }

    #[test]
    fn tune_invalid() {
        TuneBook::from_string("K:G\nX:1\n").unwrap_err();
    }
}
