use super::Mode;

#[derive(Debug, PartialEq)]
pub enum Key {
    G(Mode)
}

impl Key {
    pub fn from_str(string: &str) -> Result<Key, String> {
        use self::Key::*;
        let mut chars = string.chars();

        let key = chars.next();
        let accidental = chars.next();

        let ret = match key {
            Some(c) => {
                match c {
                    'G' => G(Mode::from_str(&string[1..])?),
                    _ => Err("Other Keys are not implemented")?
                }
            },
            None => Err("Cannot convert Key from empty string!")?
        };
        Ok(ret)
    }
}
