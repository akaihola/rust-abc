use parser::ast::Ast;
use super::Key;

#[derive(Debug, PartialEq)]
pub enum InfoField {
    ReferenceNumber(usize),
    Key(Key)
}

impl InfoField {
    pub fn from_ast(ast: &Ast) -> Result<InfoField, String> {
        use self::InfoField::*;
        use self::Key;

        let ret = match ast {
            Ast::InfoField('X', num) => {
                let num: usize = match num.trim().parse() {
                    Ok(n) => n,
                    Err(_) => Err("Reference field contains invalid content.".to_string())?
                };
                ReferenceNumber(num)
            },
            Ast::InfoField('K', key) => {
                let key = key.trim();
                Key(Key::from_str(key)?)
            },
            _ => panic!(format!("Cannot convert Ast::{:?} to InfoField", ast))
        };
        Ok(ret)
    }
}
