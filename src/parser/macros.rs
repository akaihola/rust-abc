
macro_rules! parse {
    ($name:ident($self_:ident, $ast:ident, $l:ident) $body:expr) => {
        fn $name(&mut $self_, $ast: &mut Ast) -> Result<(), String> {
            match $self_.lexer.next() {
                None => return Err(format!("Unexpected end of input in {}", stringify!($name))),
                Some($l) => match $l {
                    Malformed(_) => return Err(format!("Unexpected token in {}: {:?}", stringify!($name), $l)),
                    _ => $body
                }
            }
            Ok(())
        }
    };
    ($name:ident($self_:ident, $ast:ident, $l:ident) None => $none_body:expr, $body:expr) => {
        fn $name(&mut $self_, $ast: &mut Ast) -> Result<(), String> {
            match $self_.lexer.next() {
                None => $none_body,
                Some($l) => match $l {
                    Malformed(_) => return Err(format!("Unexpected token in $name: {:?}", $l)),
                    _ => $body
                }
            }
            Ok(())
        }
    };
}

macro_rules! reduce {
    ($name:ident($self_:ident, $ast:ident, $i:ident) $body:expr) => {
        fn $name(&mut $self_, $ast: &mut Ast) -> () {
            let mut $i = Vec::new();
            loop {
                $body
            };
        }
    };
    ($name:ident($self_:ident, $ast:ident) $body:block) => {
        fn $name(&mut $self_, $ast: &mut Ast) -> () {
            $body
        }
    };
    ($self_:ident, $reduce_fn:ident, $ast:ident, $l:ident) => {
        {
            $self_.stack.push($l);
            $self_.$reduce_fn($ast);
        }
    };
    ($self_:ident, $reduce_fn:ident, $ast:ident, $l:expr) => {
        {
            $self_.stack.push($l);
            $self_.$reduce_fn($ast);
        }
    };
}

macro_rules! shift {
    ($self_:ident, $name:ident, $ast:ident, $l:ident) => {
        {
            $self_.stack.push($l);
            $self_.$name($ast)?;
        }
    };
    ($self_:ident, $name:ident, $ast:ident, $l:expr) => {
        {
            $self_.stack.push($l);
            $self_.$name($ast)?;
        }
    };
}

macro_rules! ast_push {
    ($ast:ident, $ast_type:expr) => {{
        let children = $ast.unwrap_children();
        children.push(Box::new($ast_type));
    }};
}
