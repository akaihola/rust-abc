#[derive(Debug, PartialEq)]
pub enum Ast {
    Root(Vec<Box<Ast>>),
    GraceNotes(Vec<Box<Ast>>),
    InfoField(char, String),
    Note(String),
    Bar(String),
    Ending(String),
    Modifier(String),
    Tuplet(u8),
    ChordSymbol(String),
    VisualBreak(),
    LineBreak()
}

impl Ast {
    pub fn unwrap_children(&mut self) -> &mut Vec<Box<Ast>> {
        match self {
            Ast::Root(ref mut children) | Ast::GraceNotes(ref mut children) => children,
            _ => panic!("Wrong Ast variant found when calling unwrap_children")
        }
    }
}
