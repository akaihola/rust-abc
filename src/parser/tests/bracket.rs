use super::Parser;
use super::super::ast::Ast::*;


#[test]
fn bracket_bar_whitespace() {
    let mut parser = Parser::new("K:G\nG[ ");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("[".to_string())),
        Box::new(VisualBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bracket_bar_newline() {
    let mut parser = Parser::new("K:G\nG[\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("[".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn partial_chord() {
    let mut parser = Parser::new("K:G\nG[A");
    parser.parse().unwrap_err();
}

#[test]
fn partial_chord2() {
    let mut parser = Parser::new("K:G\nG[AB");
    parser.parse().unwrap_err();
}
