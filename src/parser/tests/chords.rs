use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn simple() {
    let mut parser = Parser::new("K:G\n\"G\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(ChordSymbol("G".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn minor() {
    let mut parser = Parser::new("K:G\n\"Gm\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(ChordSymbol("Gm".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn after_note() {
    let mut parser = Parser::new("K:G\nB\"G\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note('B'.to_string())),
        Box::new(ChordSymbol("G".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn after_bar() {
    let mut parser = Parser::new("K:G\n|\"G\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar('|'.to_string())),
        Box::new(ChordSymbol("G".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn unicode_accidentals() {
    let mut parser = Parser::new("K:G\n|\"G♭♮♯\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar('|'.to_string())),
        Box::new(ChordSymbol("G♭♮♯".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn visual_alternative() {
    let mut parser = Parser::new("K:G\n|\"G(Em)\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar('|'.to_string())),
        Box::new(ChordSymbol("G(Em)".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bass_note() {
    let mut parser = Parser::new("K:G\n|\"G/D\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar('|'.to_string())),
        Box::new(ChordSymbol("G/D".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn number() {
    let mut parser = Parser::new("K:G\n|\"Am7\"");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar('|'.to_string())),
        Box::new(ChordSymbol("Am7".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn invalid() {
    let mut parser = Parser::new("K:G\n|\"G$\"");
    parser.parse().unwrap_err();
}

#[test]
fn invalid_empty() {
    let mut parser = Parser::new("K:G\n|\"\"");
    parser.parse().unwrap_err();
}
