use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn simple() {
    let mut parser = Parser::new("K:G\n(3G3");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Tuplet(3)),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("3".to_string()))
    ]);

    assert_eq!(parsed, ast);
}
