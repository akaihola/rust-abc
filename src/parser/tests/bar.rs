use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn bar() {
    let mut parser = Parser::new("K:G\nG|\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar_no_newline() {
    let mut parser = Parser::new("K:G\nG|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar2() {
    let mut parser = Parser::new("K:G\nG|D\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(Note("D".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar3() {
    let mut parser = Parser::new("K:G\nG|]");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|]".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar4() {
    let mut parser = Parser::new("K:G\nG:|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar(":|".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar5() {
    let mut parser = Parser::new("K:G\nG|:");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|:".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar6() {
    let mut parser = Parser::new("K:G\nG[|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("[|".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar7() {
    let mut parser = Parser::new("K:G\nG]");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("]".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar_at_start() {
    let mut parser = Parser::new("|Ga Ba|");
    parser.parse().unwrap_err();
}

#[test]
fn bar_at_body_start() {
    let mut parser = Parser::new("X:1\nK:G\n|Ga Ba|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('X', "1".to_string())),
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Note("a".to_string())),
        Box::new(VisualBreak()),
        Box::new(Note("B".to_string())),
        Box::new(Note("a".to_string())),
        Box::new(Bar("|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar_after_ending() {
    let mut parser = Parser::new("X:1\nK:G\n[1 G|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('X', "1".to_string())),
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Ending("1".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}
