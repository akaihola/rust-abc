use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn info() {
    let mut parser = Parser::new("X:1\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('X', "1".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn unicode() {
    let mut parser = Parser::new("N: Copyright © Dope Beats\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('N', " Copyright © Dope Beats".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn no_newline() {
    let mut parser = Parser::new("X:1");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('X', "1".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn inline_info() {
    let mut parser = Parser::new("K:G\nG[K:A]");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(InfoField('K', "A".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn inline_info_2() {
    let mut parser = Parser::new("K:G\nGK:A\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(InfoField('K', "A".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn inline_info_3() {
    let mut parser = Parser::new("K:G\nG\\\nK:A\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(InfoField('K', "A".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn inline_info_4() {
    let mut parser = Parser::new("K:G\nG[K:]");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(InfoField('K', "".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn partial_inline_info() {
    let mut parser = Parser::new("K:G\nG[K:");
    parser.parse().unwrap_err();
}

#[test]
fn inline_after_bar() {
    let mut parser = Parser::new("K:G\nG|[K:A]");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(InfoField('K', "A".to_string())),
    ]);

    assert_eq!(parsed, ast);
}
