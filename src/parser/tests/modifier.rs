use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn prev_dot() {
    let mut parser = Parser::new("K:G\nG>\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier(">".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn double_prev_dot() {
    let mut parser = Parser::new("K:G\nG>>");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier(">>".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn multi_prev_dot() {
    let mut parser = Parser::new("K:G\nG>>>>>>");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier(">>>>>>".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn prev_dot_no_newline() {
    let mut parser = Parser::new("K:G\nG>H");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier(">".to_string())),
        Box::new(Note("H".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn post_dot() {
    let mut parser = Parser::new("K:G\n<G\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Modifier("<".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn post_dot_2() {
    let mut parser = Parser::new("K:G\nD<G\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("D".to_string())),
        Box::new(Modifier("<".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn double_post_dot() {
    let mut parser = Parser::new("K:G\n<<G");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Modifier("<<".to_string())),
        Box::new(Note("G".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn multi_post_dot() {
    let mut parser = Parser::new("K:G\n<<<<<<<G");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Modifier("<<<<<<<".to_string())),
        Box::new(Note("G".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break() {
    let mut parser = Parser::new("K:G\nG/G\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_double() {
    let mut parser = Parser::new("K:G\nG//G\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("//".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_double_2() {
    let mut parser = Parser::new("K:G\nG/2G\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Modifier("2".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_many() {
    let mut parser = Parser::new("K:G\nG///////");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("///////".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_then_bar() {
    let mut parser = Parser::new("K:G\nG/|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Bar("|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_then_bar_2() {
    let mut parser = Parser::new("K:G\nG/:|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Bar(":|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_then_bar_3() {
    let mut parser = Parser::new("K:G\nG/]|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Bar("]|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn rhythm_break_then_bar_4() {
    let mut parser = Parser::new("K:G\nG/[|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("/".to_string())),
        Box::new(Bar("[|".to_string())),
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn length_modifier() {
    let mut parser = Parser::new("K:G\nG2");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("2".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn length_modifier_then_bar() {
    let mut parser = Parser::new("K:G\nG2:|");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Modifier("2".to_string())),
        Box::new(Bar(":|".to_string()))
    ]);

    assert_eq!(parsed, ast);
}
