use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn after_bar() {
    let mut parser = Parser::new("K:G\nD|[1");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("D".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(Ending("1".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn after_bar_2() {
    let mut parser = Parser::new("K:G\nD|1");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("D".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(Ending("1".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn at_start() {
    let mut parser = Parser::new("K:G\n[1");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Ending("1".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn without_space() {
    let mut parser = Parser::new("K:G\n[1G");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Ending("1".to_string())),
        Box::new(Note("G".to_string()))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn after_bar_invalid() {
    let mut parser = Parser::new("K:G\nD| 1");
    parser.parse().unwrap_err();
}
