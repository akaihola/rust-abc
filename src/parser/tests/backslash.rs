use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn note() {
    let mut parser = Parser::new("K:G\nG\\\nA");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Note("A".to_string())),
        ]);

    assert_eq!(parsed, ast);
}

#[test]
fn bar() {
    let mut parser = Parser::new("K:G\nG|\\\nA");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("G".to_string())),
        Box::new(Bar("|".to_string())),
        Box::new(Note("A".to_string())),
    ]);

    assert_eq!(parsed, ast);
}
