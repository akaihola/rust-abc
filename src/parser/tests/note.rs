use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn note() {
    let mut parser = Parser::new("K:G\nA\n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("A".to_string())),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn not_unicode() {
    let mut parser = Parser::new("K:G\n©\n");
    parser.parse().unwrap_err();
}

#[test]
fn with_space() {
    let mut parser = Parser::new("K:G\nB \n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("B".to_string())),
        Box::new(VisualBreak()),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn with_space_2() {
    let mut parser = Parser::new("K:G\nC      \n");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("C".to_string())),
        Box::new(VisualBreak()),
        Box::new(LineBreak())
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn no_newline() {
    let mut parser = Parser::new("K:G\nD");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("D".to_string()))
    ]);

    assert_eq!(parsed, ast);
}
