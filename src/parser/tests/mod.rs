use super::Parser;

mod backslash;
mod bar;
mod bracket;
mod chords;
mod endings;
mod grace_notes;
mod info;
mod modifier;
mod note;
mod tuplets;


#[test]
fn malformed() {
    let mut parser = Parser::new("\0");
    parser.parse().unwrap_err();
}
