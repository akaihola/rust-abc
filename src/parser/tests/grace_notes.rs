use super::Parser;
use super::super::ast::Ast::*;

#[test]
fn simple() {
    let mut parser = Parser::new("K:G\n{G}");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(GraceNotes(vec![
            Box::new(Note("G".to_string()))
        ]))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn multiple() {
    let mut parser = Parser::new("K:G\n{GaD}");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(GraceNotes(vec![
            Box::new(Note("G".to_string())),
            Box::new(Note("a".to_string())),
            Box::new(Note("D".to_string()))
        ]))
    ]);

    assert_eq!(parsed, ast);
}

#[test]
fn after_modifier() {
    let mut parser = Parser::new("K:G\nc<{e}");
    let parsed = parser.parse().unwrap();
    let ast = Root(vec![
        Box::new(InfoField('K', "G".to_string())),
        Box::new(Note("c".to_string())),
        Box::new(Modifier("<".to_string())),
        Box::new(GraceNotes(vec![
            Box::new(Note("e".to_string()))
        ]))
    ]);

    assert_eq!(parsed, ast);
}
