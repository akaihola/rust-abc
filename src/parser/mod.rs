use super::lexer::Lexer;
use super::lexer::tokens::Token;
use super::lexer::tokens::Token::*;

use std::iter::Peekable;

pub mod ast;
#[macro_use] mod macros;

use self::ast::Ast;

pub struct Parser<'a> {
    lexer: Peekable<Lexer<'a>>,
    stack: Vec<Token>
}

impl<'a> Parser<'a> {
    pub fn new(tune_string: &'a str) -> Parser {
        Parser {
            lexer: Lexer::new(tune_string).peekable(),
            stack: vec![]
        }
    }

    pub fn print_lexemes(self) {
        for lexeme in self.lexer {
            match lexeme {
                Malformed(_) => {
                    println!("Found MALFORMED {:?}", lexeme);
                },
                _ => {
                    println!("Found Token {:?}", lexeme);
                }
            }
        }
    }

    pub fn parse(&mut self) -> Result<Ast, String> {
        let mut ast = Ast::Root(vec![]);
        self.parse_ast(&mut ast)?;
        Ok(ast)
    }

    fn parse_ast(&mut self, ast: &mut Ast) -> Result<(), String> {
        loop {
            println!("Ast {:?}", ast);
            match self.lexer.next() {
                None => break,
                Some(l) => match l {
                    Whitespace(_) | LineBreak() => (),
                    Letter('K') => {
                        self.stack.push(l);
                        self.parse_header_letter_k(ast)?;
                        self.parse_body(ast)?;
                    },
                    Letter(_) => {
                        self.stack.push(l);
                        self.parse_header_letter(ast)?;
                    },
                    _ => return Err(format!("Unexpected token in parse_ast {:?}", l))
                }
            }
        }
        Ok(())
    }

    fn parse_body(&mut self, ast: &mut Ast) -> Result<(), String> {
        loop {
            match self.lexer.next() {
                None => break,
                Some(l) => match l {
                    Whitespace(_) => (),
                    LineBreak() => self.reduce_newline(ast),
                    Letter('X') => {
                        self.stack.push(l);
                        self.parse_letter_x(ast)?;
                        self.parse_ast(ast)?;
                    }
                    Letter(_) => shift!(self, parse_letter, ast, l),
                    Punctuation(':') | Punctuation('|') | Punctuation(']') => shift!(self, parse_bar, ast, l),
                    Punctuation('[') => shift!(self, parse_bracket, ast, l),
                    Punctuation('"') => shift!(self, parse_chord_symbol, ast, l),
                    Punctuation('<') => shift!(self, parse_modifier, ast, l),
                    Punctuation('(') => shift!(self, parse_paren, ast, l),
                    Punctuation('{') => shift!(self, parse_grace_notes, ast, l),
                    _ => return Err(format!("Unexpected token in parse_body {:?}", l))
                }
            }
        }
        Ok(())
    }

    fn parse_grace_notes(&mut self, ast: &mut Ast) -> Result<(), String> {
        let mut grace_ast = Ast::GraceNotes(vec![]);
        loop {
            match self.lexer.peek().ok_or("Unexpected end of input")? {
                Punctuation('}') => break,
                _ => ()
            }
            self.parse_note(&mut grace_ast)?;
        }
        // Consume the '}'
        self.lexer.next();
        ast.unwrap_children().push(Box::new(grace_ast));
        Ok(())
    }

    fn parse_note(&mut self, ast: &mut Ast) -> Result<(), String> {
        match self.lexer.next() {
            None => Err(format!("Unexpected end of input in parse_note!"))?,
            Some(l) => match l {
                Letter('a'...'g') | Letter('A'...'G') => reduce!(self, reduce_note, ast, l),
                _ => Err(format!("Unexpected token in parse_note {:?}", l))?
            }
        }
        Ok(())
    }

    parse! {
        parse_backslash(self, ast, l) None => return Ok(()),
        match l {
            Whitespace(_) => self.parse_backslash_whitespace(ast)?,
            Letter(c) => Err(format!("Illegal escape character {:?}", c))?,
            LineBreak() => return Ok(()),
            _ => Err(format!("Illegal token following backslash {:?}", l))?
        }
    }

    parse! {
        parse_backslash_whitespace(self, ast, l) None => return Ok(()),
        match l {
            Whitespace(_) => self.parse_backslash_whitespace(ast)?,
            LineBreak() => return Ok(()),
            _ => Err(format!("Illegal token following backslash {:?}", l))?
        }
    }

    parse! {
        parse_header_letter(self, ast, l)
        match l {
            Punctuation(':') => shift!(self, parse_header_letter_colon, ast, l),
            _ => Err(format!("Expecting ':' found {:?}", l.repr().iter().collect::<String>()))?
        }
    }

    parse! {
        parse_header_letter_k(self, ast, l)
        match l {
            Punctuation(':') => shift!(self, parse_header_letter_k_colon, ast, l),
            _ => Err(format!("Expecting ':' found {:?}", l.repr().iter().collect::<String>()))?
        }
    }

    parse! {
        parse_header_letter_colon(self, ast, l) None => self.reduce_info(ast),
        match l {
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_header_info, ast, l)
        }
    }

    parse! {
        parse_header_letter_k_colon(self, ast, l) None => self.reduce_info(ast),
        match l {
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_header_info_k, ast, l)
        }
    }

    parse! {
        parse_header_info(self, ast, l) None => self.reduce_info(ast),
        match l {
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_header_info, ast, l)
        }
    }

    parse! {
        parse_header_info_k(self, ast, l) None => self.reduce_info(ast),
        match l {
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_header_info_k, ast, l)
        }
    }

    parse! {
        parse_paren(self, ast, l)
        match l {
            Number(_) => reduce!(self, reduce_tuplet, ast, l),
            _ => Err(format!("Unexpected token in parse_paren {:?} expecting Number(_)", l))?
        }
    }

    parse! {
        parse_bracket(self, ast, l) None => self.reduce_bar(ast),
        match l {
            Punctuation('|') | Punctuation(':') |
            Punctuation(']') => shift!(self, parse_bar, ast, l),
            Letter(_) => shift!(self, parse_bracket_letter, ast, l),
            Number(_) => {
                self.stack.pop().unwrap();
                self.stack.push(l);
                self.reduce_ending(ast);
            },
            Whitespace(_) => {
                self.reduce_bar(ast);
                self.stack.push(l);
                self.reduce_whitespace(ast);
            },
            LineBreak() => {
                self.reduce_bar(ast);
                self.reduce_newline(ast)
            }
            _ => Err(format!("Unexpected token in parse_bracket {:?}", l))?
        }
    }

    parse! {
        parse_bracket_letter(self, ast, l)
        match l {
            Letter(_) | Number(_) => shift!(self, parse_note_stack, ast, l),
            Punctuation(':') => shift!(self, parse_inline_info, ast, l),
            _ => Err(format!("Unexpected token in parse_bracket_letter {:?}", l))?
        }
    }

    parse! {
        parse_letter_x(self, ast, l)
        match l {
            Punctuation(':') => shift!(self, parse_letter_colon, ast, l),
            _ => Err(format!("Unexpected token in parse_letter_x {:?} expecting ':'", l))?
        }
    }

    parse! {
        parse_letter(self, ast, l) None => self.reduce_note(ast),
        match l {
            Punctuation(':') => shift!(self, parse_letter_colon, ast, l),
            _ => {
                self.reduce_note(ast);
                match l {
                    Letter(_) => shift!(self, parse_letter, ast, l),
                    Number(_) => reduce!(self, reduce_modifier, ast, l),
                    Punctuation('<') | Punctuation('>') | Punctuation('/') => shift!(self, parse_modifier, ast, l),
                    Punctuation('|') | Punctuation(']') => shift!(self, parse_bar, ast, l),
                    Punctuation('[') => shift!(self, parse_bracket, ast, l),
                    Punctuation('"') => shift!(self, parse_chord_symbol, ast, l),
                    Whitespace(_) => self.reduce_whitespace(ast),
                    LineBreak() => self.reduce_newline(ast),
                    Punctuation('\\') => self.parse_backslash(ast)?,
                    _ => Err(format!("Unexpected token in parse_letter {:?}", l))?
                }
            }
        }
    }

    parse! {
        parse_letter_colon(self, ast, l) None => self.reduce_info(ast),
        match l {
            Punctuation('|') => {
                let prev = self.stack.pop().unwrap();
                self.reduce_note(ast);
                self.stack.push(prev);
                shift!(self, parse_bar, ast, l)
            },
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_info, ast, l)
        }
    }

    parse! {
        parse_modifier(self, ast, l) None => self.reduce_modifier(ast),
        match l {
            Punctuation('|') | Punctuation(':') | Punctuation(']') => {
                self.reduce_modifier(ast);
                shift!(self, parse_bar, ast, l);
            },
            Punctuation('[') => {
                self.reduce_modifier(ast);
                shift!(self, parse_bracket, ast, l);
            }
            Punctuation('{') => {
                self.reduce_modifier(ast);
                shift!(self, parse_grace_notes, ast, l);
            }
            Punctuation(c) if c == '<' || c == '>' || c == '/' => {
                match self.stack.last().unwrap() {
                    &Punctuation(prev_c) => {
                        if prev_c != c {
                            self.reduce_modifier(ast);
                        }
                        shift!(self, parse_modifier, ast, l);
                    },
                    _ => panic!["Unexpected token on stack in parse_modifier"]
                }
            },
            Letter(_) => {
                self.reduce_modifier(ast);
                shift!(self, parse_letter, ast, l)
            },
            Number(_) => {
                self.reduce_modifier(ast);
                shift!(self, parse_modifier, ast, l)
            }
            LineBreak() => {
                self.reduce_modifier(ast);
                self.reduce_newline(ast);
            }
            _ => Err(format!("Unexpected token in parse_modifier {:?}", l))?
        }
    }

    parse! {
        parse_info(self, ast, l) None => self.reduce_info(ast),
        match l {
            LineBreak() => self.reduce_info(ast),
            _ => shift!(self, parse_info, ast, l)
        }
    }

    parse! {
        parse_inline_info(self, ast, l)
        match l {
            Punctuation(']') => {
                self.reduce_info(ast);
                self.stack.pop().unwrap(); // Pop off the starting bracket
            },
            _ => shift!(self, parse_inline_info, ast, l)
        }
    }

    parse! {
        parse_bar(self, ast, l) None => self.reduce_bar(ast),
        match l {
            Punctuation('|') | Punctuation(']') | Punctuation(':') => shift!(self, parse_bar, ast, l),
            Punctuation('[') => shift!(self, parse_bar_bracket, ast, l),
            _ => {
                self.reduce_bar(ast);
                match l {
                    Punctuation('>') => reduce!(self, reduce_modifier, ast, l),
                    Punctuation('"') => shift!(self, parse_chord_symbol, ast, l),
                    Letter(_) => shift!(self, parse_letter, ast, l),
                    Number(_) => reduce!(self, reduce_ending, ast, l),
                    Whitespace(_) => self.reduce_whitespace(ast),
                    LineBreak() => self.reduce_newline(ast),
                    Punctuation('\\') => self.parse_backslash(ast)?,
                    _ => Err(format!("Unexpected token in parse_bar {:?}", l))?
                }
            }
        }
    }

    parse! {
        parse_bar_bracket(self, ast, l) None => self.reduce_bar(ast),
        match l {
            Number(_) => {
                self.stack.pop().unwrap();
                self.reduce_bar(ast);
                reduce!(self, reduce_ending, ast, l)
            },
            Letter(_) => {
                let bracket = self.stack.pop().unwrap();
                self.reduce_bar(ast);
                self.stack.push(bracket);
                shift!(self, parse_inline_info, ast, l)
            }
            _ => Err(format!("Unexpected token in parse_bar_bracket {:?}", l))?
        }
    }

    parse! {
        parse_note_stack(self, _ast, l)
        match l {
            _ => Err("Not Implemented".to_string())?
        }
    }

    parse! {
        parse_chord_symbol(self, ast, l)
        match l {
            Punctuation('"') => Err("Found empty chord symbol!".to_string())?,
            Letter('A'...'G') => shift!(self, parse_chord_symbol_letter, ast, l),
            _ => Err(format!("Unexpected token in parse_chord_symbol {:?} Expecting A-G", l))?
        }
    }

    parse! {
        parse_chord_symbol_letter(self, ast, l)
        match l {
            Punctuation('"') => self.reduce_chord_symbol(ast),
            Letter('a'...'z') | Letter('A'...'G') | Number(_) |
            Unicode('♭') | Unicode('♮') | Unicode('♯') |
            Punctuation('(') | Punctuation(')') | Punctuation('/') =>
                shift!(self, parse_chord_symbol_letter, ast, l),
            _ => Err(format!("Unexpected token in parse_chord_symbol_letter {:?} Expecting lowercase letters, 7, 9, b, #, or +", l))?
        }
    }

    reduce! {
        reduce_info(self, ast, string) match self.stack.pop().unwrap() {
            Punctuation(':') => {
                if let Letter(c) = self.stack.pop().unwrap() {
                    ast_push!(ast, Ast::InfoField(c, string.iter().rev().collect()));
                    break;
                }
                panic!("Internal error: Unexpected token while reducing")
            }
            lexeme => string.append(&mut lexeme.repr())
        }
    }

    reduce! {
        reduce_note(self, ast, string) match self.stack.pop().unwrap() {
            Letter(c) => {
                string.push(c);
                ast_push!(ast, Ast::Note(string.iter().rev().collect()));
                break;
            },
            lexeme => string.append(&mut lexeme.repr())
        }
    }

    reduce! {
        reduce_modifier(self, ast) {
            // Find out what type of modifier we are
            match self.stack.pop().unwrap() {
                Punctuation(first) => {
                    let mut string = vec![first];
                    loop {
                        match self.stack.pop() {
                            None => {
                                ast_push!(ast, Ast::Modifier(string.iter().collect()));
                                break;
                            },
                            Some(Punctuation(c)) if c != first => {
                                self.stack.push(Punctuation(c));
                                ast_push!(ast, Ast::Modifier(string.iter().collect()));
                                break;
                            },
                            Some(lexeme) => string.append(&mut lexeme.repr())
                        }
                    }
                },
                Number(n) => ast_push!(ast, Ast::Modifier(n)),
                _ => panic!["Wrong token found in reduce_modifier"]
            }
        }
    }

    reduce! {
        reduce_whitespace(self, ast) {
            ast_push!(ast, Ast::VisualBreak());
        }
    }

    reduce! {
        reduce_newline(self, ast) {
            ast_push!(ast, Ast::LineBreak());
        }
    }

    reduce! {
        reduce_ending(self, ast) {
            let lexeme = self.stack.pop().unwrap();
            ast_push!(ast, Ast::Ending(lexeme.repr().iter().collect()));
        }
    }

    reduce! {
        reduce_bar(self, ast, string) match self.stack.pop() {
            Some(Punctuation(c)) if !(c == '|' || c == '[' || c == ']' || c == ':') => {
                // Undo the pop, because this lexeme is not part of the bar
                self.stack.push(Punctuation(c));
                ast_push!(ast, Ast::Bar(string.iter().rev().collect()));
                break;
            },
            None => {
                ast_push!(ast, Ast::Bar(string.iter().rev().collect()));
                break;
            }
            Some(Punctuation(c)) => string.push(c),
            _ => panic!["Wrong token found in reduce_bar"]
        }
    }

    reduce! {
        reduce_chord_symbol(self, ast, string) match self.stack.pop().unwrap() {
            Punctuation('"') => {
                ast_push!(ast, Ast::ChordSymbol(string.iter().rev().collect()));
                break;
            },
            lexeme => string.append(&mut lexeme.repr())
        }
    }

    reduce! {
        reduce_tuplet(self, ast) {
            let n = self.stack.pop().unwrap().unwrap_number()
                .parse().unwrap();
            ast_push!(ast, Ast::Tuplet(n));
        }
    }
}

#[cfg(test)]
mod tests;
