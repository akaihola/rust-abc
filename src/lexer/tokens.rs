#[derive(Debug, PartialEq)]
pub enum Token {
    Whitespace(char),
    LineBreak(),
    Punctuation(char),
    Number(String),
    Letter(char),
    Unicode(char),
    Malformed(char)
}

impl Token {
    pub fn repr(&self) -> Vec<char> {
        use self::Token::*;
        match self {
            Whitespace(c) | Punctuation(c) | Letter(c) | Unicode(c) | Malformed(c) => vec![*c],
            Number(s) => s.chars().collect(),
            LineBreak() => vec!['\n'] // Sorry windows ;)
        }
    }

    pub fn unwrap_number(&self) -> &str {
        match self {
            Token::Number(n) => &n,
            _ => panic!("Wrong token variant found when calling unwrap_number")
        }
    }
}
