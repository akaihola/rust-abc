pub mod tokens;

use std::str::Chars;
use std::iter::Peekable;

use self::tokens::Token;
use self::tokens::Token::*;

pub struct Lexer<'a> {
    input: Peekable<Chars<'a>>
}

impl<'a> Lexer<'a> {
    pub fn new(string: &'a str) -> Lexer {
        Lexer {
            input: string.chars().peekable()
        }
    }

    fn lex(&mut self) -> Option<Token> {
        let c = self.input.next()?;
        match c {
            '%' => {
                self.skip_comment();
                self.lex()
            },
            ' ' | '\t' => Some(Whitespace(c)),
            '\n' => Some(LineBreak()),
            '\r' => Some(self.lex_windows_style_newline()),
            'a' ... 'z' | 'A' ... 'Z' => Some(Letter(c)),
            '0' ... '9' => Some(self.lex_number(c.to_string())),
            '!'...'/' | ':'...'@' | '['...'`' | '{'...'~' => Some(Punctuation(c)),
            a if !a.is_ascii() => Some(Unicode(a)),
            a => Some(Malformed(a))
        }
    }

    fn skip_comment(&mut self) {
        match self.input.peek() {
            None => return,
            Some(&c) => match c {
                '\n' => return,
                _ => {
                    self.input.next();
                    self.skip_comment()
                }
            }
        }
    }

    fn lex_number(&mut self, mut current: String) -> Token {
        match self.input.peek() {
            None => Number(current),
            Some(&c) => match c {
                '0' ... '9' => {
                    self.input.next();
                    current.push(c);
                    self.lex_number(current)
                },
                _ => Number(current)
            }
        }
    }

    fn lex_windows_style_newline(&mut self) -> Token {
        match self.input.peek() {
            None => LineBreak(),
            Some(&c) => match c {
                '\n' => {
                    self.input.next();
                    LineBreak()
                },
                _ => {
                    LineBreak()
                }
            }
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;

    fn next(self: &mut Self) -> Option<Self::Item> {
        self.lex()
    }
}

#[cfg(test)]
mod tests {
    use super::Lexer;
    use super::tokens::Token;
    use super::tokens::Token::*;

    #[test]
    fn simple() {
        let lexer = Lexer::new("X:1\n");
        let lexed: Vec<Token> = lexer.collect();
        let lexemes = vec![
            Letter('X'),
            Punctuation(':'),
            Number("1".to_string()),
            LineBreak()
        ];

        assert_eq!(lexed, lexemes);
    }

    #[test]
    fn all_types() {
        let lexer = Lexer::new("G3a,[ \n|12❤\0");
        let lexed: Vec<Token> = lexer.collect();
        let lexemes = vec![
            Letter('G'),
            Number("3".to_string()),
            Letter('a'),
            Punctuation(','),
            Punctuation('['),
            Whitespace(' '),
            LineBreak(),
            Punctuation('|'),
            Number("12".to_string()),
            Unicode('❤'),
            Malformed('\0')
        ];

        assert_eq!(lexed, lexemes);
    }

    #[test]
    fn backslash() {
        let lexer = Lexer::new("A\\\nB");
        let lexed: Vec<Token> = lexer.collect();
        let lexemes = vec![
            Letter('A'),
            Punctuation('\\'),
            LineBreak(),
            Letter('B'),
        ];

        assert_eq!(lexed, lexemes);
    }

    #[test]
    fn windows_style_newline() {
        let lexer = Lexer::new("A\r\nB");
        let lexed: Vec<Token> = lexer.collect();
        let lexemes = vec![
            Letter('A'),
            LineBreak(),
            Letter('B'),
        ];

        assert_eq!(lexed, lexemes);
    }
}
