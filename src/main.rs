extern crate abc_parser;

use std::fs::File;
use std::io::prelude::*;
use std::env;
use abc_parser::parser::Parser;
use std::process;

struct Config {
    file_name: String
}

fn main() {
    let config = match parse_config(env::args()) {
        Ok(c) => c,
        Err(s) => {
            println!("Error: {}", s);
            process::exit(-1);
        }
    };

    let mut file = File::open(config.file_name).unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();

    let mut parser = Parser::new(&contents);

    // parser.print_lexemes();

    println!("Parse result {:?}", parser.parse());
}

fn parse_config(mut args: env::Args) -> Result<Config, &'static str> {
    args.next();

    let file_name = match args.next() {
        Some(s) => s,
        None => return Err("Missing filename")
    };

    Ok(Config { file_name })
}
